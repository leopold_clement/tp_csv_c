#ifndef HASH_H
#define HASH_H
#include <stdint.h>

#define HASH_SIZE 16
int hash_file(char* path_name, uint8_t hash[HASH_SIZE]);

#endif