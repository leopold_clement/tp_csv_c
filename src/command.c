#include <stdlib.h>
#include <stdio.h>

#include "command.h"
#include "hash.h"

const char* usage="Hash file\n\
version 0.1.0\n\
usage : \n\
    hashfile -f <path_to_file>\n\
        return the hash of the file\n\
    hashfile -d <path_to _directory>\n\
        return a csv of the hash of the file in the directory\n\
    hashfile -c <path_to_directory> <path_to_csv>\n\
        check that the directory math the csv\n";

void version(int exitcode){
    printf("%s\n", usage);
    exit(exitcode);
}

int main(int argc, char const *argv[])
{
    if (argc == 1) version(1);
    if (argv[1][0] != '-') version(1);
    switch (argv[1][1])
    {
    case 'f':
        // code 1
        break;
    case 'c':
        // code 2
        break;
    case 'd':
        // code 3
        break;
    case 'v':
        version(0);
    default:
        version(1);
    }
    return 0;
}